<?php
/**
 * Plugin Name: BurdaForward Content Platform Adapter
 * Plugin URI: https://www.burda-forward.de/
 * Description: Generiert einen RSS-Feed, der den Import veröffentlichter Artikel in die BurdaForward Content Platform erlaubt
 * Author: BurdaForward
 * Version: 0.0.1
 * License: MIT
 */

// Load Plugin and fire customRSS()
add_action('init', 'customRSS');



// Create new feed type cpfeed that can be reached via feed/cpfeed
// When this feed is requested, fire customRSSFunc()
function customRSS() {
    add_feed('cpfeed', 'customRSSFunc');
}


// This will look for a template file called rss-cpfeed.php
function customRSSFunc() {
    // get_template_part('rss', 'cpfeed');
	if ($feed_template = locate_template( 'rss-cpfeed.php')) {
		load_template($feed_template);
	} else {
		load_template(dirname( __FILE__ ) . '/feed/rss-cpfeed.php');
	}
}

function bf_attachFeaturedImage() {

	global $post;

	$attachments = get_posts( array(
		'post_type' => 'attachment',
		'posts_per_page' => -1,
		'post_parent' => $post->ID,
		'include' => get_post_thumbnail_id($post->ID)
	) );

	foreach ( $attachments as $att ) {
		$img = wp_get_attachment_image_src( $att->ID, 'thumbnail' );
		if ($img) {
			$filesize = filesize(get_attached_file($att->ID));
			?>
			<media:content url="<?php echo esc_url($img[0]); ?>" fileSize="<?php echo $filesize; ?>" type="<?php echo esc_attr($att->post_mime_type); ?>" medium="image" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>">
				<media:title type="plain"><![CDATA[<?php echo $att->post_title; ?>]]></media:title>
				<media:description type="plain"><![CDATA[<?php echo $att->post_title; ?>]]></media:description>
				<media:credit role="photographer" scheme="urn:ebu"><?php echo $att->credit; ?></media:credit>
			</media:content>
			<?php
		}
	}
}
add_filter( 'rss2_item', 'bf_attachFeaturedImage' );


function bf_editFeed($content) {

    // Feed einlesen
    $doc = bf_getFeedContent($content);

    // Bilder finden
    $images = $doc->getElementsByTagName('img');
    if (!$images) {
	    return $content;
    }

    // Alle Bilder durchlaufen, IDs herausfinden und HTML mit Bild + Credit zurückgeben
    $imgout = array();
    foreach ($images as $image) {
        $url = $image->getAttribute('src');
        $id = bf_getImageID($url);
        $credit = get_post_meta($id, 'credit', true);

        $imgout[] = '<div class="credit"><img src="' . $url . '"><p>' . $credit . '</p></div>';

    }

    return $imgout;

}


function bf_getFeedContent($content) {
    $content = '<div>' . $content . '</div>';
    $blogpost = new DOMDocument();
    // TODO: Feed konvertieren, falls Probleme mit Encoding auftreten
    $blogpost->loadHTML($content);

    return $blogpost;

}


function bf_getImageID($imgurl) {



    // Attachment-ID herausfinden
    if (function_exists('attachment_url_to_postid')) {
        $id = attachment_url_to_postid($imgurl);
        return $id > 0 ? $id : false;
    }

    // TODO: Fallback, wenn attachment_url_to_postid nicht verfügbar ist
    return false;




}


function bf_addCreditField( $form_fields, $post ) {
	$form_fields['credit'] = array(
		'label' => 'Credit',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'credit', true ),
		'helps' => 'Urheber*in des Bildes',
	);

	return $form_fields;
}

add_filter( 'attachment_fields_to_edit', 'bf_addCreditField', 10, 2 );


function bf_saveCreditField( $post, $attachment ) {
	if( isset( $attachment['credit'] ) )
		update_post_meta( $post['ID'], 'credit', $attachment['credit'] );

	return $post;
}

add_filter( 'attachment_fields_to_save', 'bf_saveCreditField', 10, 2 );