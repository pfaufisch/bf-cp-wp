<?php
/**
 * BF CP Adapater RSS-Template
 */

const ALLOWABLE_TAGS = '<a><b><h2><blockquote><cite><code><dd><div><dl><dt><em><i><li><ol><p><pre><q><small><span><strike><strong><sub><sup><u><ul><img><table><tbody><td><tfoot><th><thead><tr>';

$postCount = 5; // The number of posts to show in the feed
$posts = query_posts('showposts=' . $postCount);

header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';

?>

<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">
    <channel>
        <title><?php wp_title_rss(); ?></title>
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss("description") ?></description>


		<?php while( have_posts()) : the_post(); ?>
            <item>
                <title><?php the_title_rss() ?></title>
                <link><?php the_permalink_rss() ?></link>
                <description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
				<?php
                    // $content = get_the_content_feed('rss2');
                    $content = get_the_content(null, true);
                    $content = strip_tags($content, ALLOWABLE_TAGS);
                    $postID = get_the_ID();
				    $credit = get_post_meta($postID, 'credit', true);
				    $postMeta = get_post_meta($postID);
                    ?>
                <content:encoded>
                    <![CDATA[
					<?php
                        echo $content;
					    $images = bf_editFeed($content);
					    foreach ($images as $image) {
					        echo $image;
                        }
					?>
                    ]]>
                </content:encoded>
                <pubDate><?php echo mysql2date('D, d M y H:m:s +Z', get_post_time('U', true), true); ?></pubDate>
                <author><?php the_author() ?></author>
                <guid isPermaLink="true"><?php the_guid(); ?></guid>
                <?php
                do_action( 'rss2_item' );
                ?>
            </item>
		<?php endwhile; ?>
    </channel>
</rss>